/* the following java program demonstrate how to compare the state of two objects without using comparator */ 
import java.lang.reflect.Field;//this is used to get the fields of a particular class
import java.lang.Class;//to collect the class object from getClass()
public class Demo
{
public static boolean compare(Object o,Object p)throws IllegalAccessException //this method can throw illegal access exception in following code while accessing fields
{
//we have created a static method because it can be called without creating an object by using its class name
boolean flag = false;//this is used to know state of an object
 
try
{
if (o==null || p==null)//if null return false
return false;
 if(o.getClass().equals(p.getClass())) //check whether two objects are of same class
{
System.out.println("Inside get class equals method");
Field[]  field = o.getClass().getDeclaredFields();
for(Field f:field) //to traverse through all attributes
{

f.setAccessible(true);
if(f.get(o).equals(f.get(p))){//to compare values of both the classes

flag = true;

}
else  //break the loop if values does not match
{
flag = false;
break;
}

}
}
else if(o.getClass().getSuperclass().equals(p.getClass()))//compare hierachial objects
{
System.out.println("Inside super class method");
//Class cls = o.getClass();
//cls= cls.getSuperclass();

Field[]  fi = o.getClass().getDeclaredFields();
Field[]  fii = p.getClass().getDeclaredFields();
System.out.println("Inside super class method"+fi.length);
System.out.println("Inside super class method"+fii.length);
if(fi.length != fii.length){ //if no of attributes of classes does not match return false 
flag = false;



}
else//if no of attributes of two classes  matche then compare values
{
fi = o.getClass().getFields();
//Field[]  fii = p.getClass().getDeclaredFields();
for(Field f:fi)
{
f.setAccessible(true);
System.out.println("Inside super class method");

if(f.get(o).equals(f.get(p))){
System.out.println("Inside super class method");

flag = true;

}
else
{
flag = false;
break;
}

//cls = cls.getSuperclass();
}
}
}
else if(o.getClass()!= p.getClass()  ) //if two objects are of different classes then we should assign false
{
flag = false;

}
else
{


}
}
catch(NullPointerException e)//thrown if user passes null object
{

System.out.println("u have passed a null object please make sure that u create an object and pass its reference");
return false;


}
catch(IllegalArgumentException e)//thrown when user passes wrong parameter
{

System.out.println("please pass correct objects namely objects of particular classes");
}
catch(IllegalAccessException e)//thrown while accessing private members of a class
{

System.out.println("you are trying to access some illegal attributes");
}
catch(Exception e)
{

System.out.println("some error has occured");
}

if(flag){//true if both states of objects match

System.out.println("Same state");
}
else//if both states of objects does not match
{
System.out.println("Not in the Same state");
}
return flag;

}

}
**********************************
//class A
public class A
{
int a,b;
A()
{


}
A(int a,int b)
{

this.a = a;
this.b = b;

}


}
//class B
class B
{
int a,b;
B(int a,int b)
{

this.a = a;
this.b = b;

}


}
*******************************
//Base class which will be inherited by Child class
class Base
{
int a,b;
Base()
{


}
Base(int a,int b)
{
this.a = a;
this.b= b;

}



}
*****************************
//Child class which extends Base class
class Child extends Base
{

int a,b,c,d;
Child(int a,int b,int c,int d)
{

super(a,b);
this.c = c;
this.d = d;

}



}
**********************************************
// launcher class used to call compare method of Demo class
public class C
{
public static void main(String args[])throws Exception
{

A ob = new A(10,20);
A ob1 = new A(20,30);
B ob2 = new B(10,20);
B ob3 = new B(10,20);
Base ob4 = new Base(10,20);
Child ob5 = new Child(10,20,30,40);


System.out.println("are two object states equal ? "+" "+Demo.compare(null,ob3));
}



}